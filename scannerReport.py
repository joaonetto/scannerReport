from argparse import ArgumentParser
from copy import deepcopy
from datetime import datetime
from dateutil import parser
from jinja2 import Environment, FileSystemLoader, select_autoescape
from json import load
from os import path
import logging


logger = logging.getLogger()
logger.setLevel(logging.INFO)


def date_format() -> str:
    return "%B, %d %Y at %H:%M"


def scanner_type(raw: dict) -> dict:
    print('-> Understanding what scanner are in use.')
    image_type = str()
    scanner = dict()

    os = dict()
    if 'SchemaVersion' in raw and \
            'ArtifactName' in raw and \
            'ArtifactType' in raw and \
            'Metadata' in raw and \
            'Results' in raw:
        if raw.get('ArtifactType') == 'container_image':
            image_type = 'Container'
            os = raw.get('Metadata').get('OS')
            os['os'] = raw.get('Metadata').get('ImageConfig').get('os')
            os['arch'] = raw.get('Metadata').get('ImageConfig').get('architecture')
        print('--> Scanner is: Trivy.')
        scanner = {
            'ImageType': image_type,
            'ImageID': raw.get('Metadata').get('ImageID'),
            'ArtifactName': raw.get('ArtifactName'),
            'ArtifactType': raw.get('ArtifactType'),
            'ScannerPlatform': 'Trivy',
            'OS': os,
            'Created': datetime.strftime(
                parser.parse(raw.get('Metadata').get('ImageConfig').get('created')
                             ), date_format())

        }

    return scanner


def split_info_scanner(raw: dict) -> dict:
    packages = list()
    secrets = list()
    layers = list()
    layers_resume = 0
    resume_secrets = int()
    print('-> Split data in Json.')
    if raw.get('ArtifactType') == 'container_image':
        print('--> This scanner are working with: Container.')
        for each_item in raw.get('Results'):
            if each_item.get('Class') == 'os-pkgs' or each_item.get('Class') == 'lang-pkgs':
                if 'Vulnerabilities' in each_item:
                    packages.append(each_item)
            elif each_item.get('Class') == 'secret':
                secrets.append(each_item)
                resume_secrets = resume_secrets + len(each_item.get('Secrets'))

    if 'history' in raw.get('Metadata').get('ImageConfig'):
        for each_layer in raw.get('Metadata').get('ImageConfig').get('history'):
            layers.append(each_layer.get('created_by'))
            layers_resume = len(layers)

    return {
        'Packages': {
            'rawResults': packages,
            'Resume': {}
        },
        'Secrets': {
            'rawResults': secrets,
            'Resume': resume_secrets
        },
        'Layers': {
            'rawResults': layers,
            'Resume': layers_resume
        }
    }


def concern_findings(raw: dict) -> dict:
    amount_vuln = {
        "CRITICAL": 0,
        "HIGH": 0,
        "MEDIUM": 0,
        "LOW": 0
    }
    print('-> Searching for vulnerabilities.')
    for each_item in raw.get('Packages').get('rawResults'):
        for each_vuln in each_item.get('Vulnerabilities'):
            amount_vuln[each_vuln.get('Severity')] += 1
        raw['Packages']['Resume'] = amount_vuln

    print(f'--> Some vulnerabilites was found.')
    print(f'---> Critical: {amount_vuln.get("CRITICAL")}.')
    print(f'--->     HIGH: {amount_vuln.get("HIGH")}.')
    print(f'--->   Medium: {amount_vuln.get("MEDIUM")}.')
    print(f'--->      Low: {amount_vuln.get("LOW")}.')

    return raw


def collect_score(vulnerability: dict) -> str:
    if 'CVSS' in vulnerability:
        if 'nvd' in vulnerability.get('CVSS'):
            if 'V3Score' in vulnerability.get('CVSS').get('nvd'):
                score = f"nvd / V3Score / {vulnerability.get('CVSS').get('nvd').get('V3Score')}"
            else:
                score = f"nvd / V2Score / {vulnerability.get('CVSS').get('nvd').get('V2Score')}"
        else:
            if 'SeveritySource' in vulnerability.get('CVSS'):
                if 'V3Score' in vulnerability.get('CVSS').get(vulnerability.get('SeveritySource')):
                    score = f"{vulnerability.get('SeveritySource')} / " \
                            f"V3Score / " \
                            f"{vulnerability.get('CVSS').get(vulnerability.get('SeveritySource')).get('V3Score')}"
                else:
                    score = f"{vulnerability.get('SeveritySource')} / " \
                            f"V2Score / " \
                            f"{vulnerability.get('CVSS').get(vulnerability.get('SeveritySource')).get('V2Score')}"
            else:
                score = 'N/A'
    else:
        score = 'N/A'

    return str(score)


def select_package_name(raw: dict) -> dict:
    print('-> Order by Package Name.')
    pkg_list = list()
    for each_result in raw.get('Packages').get('rawResults'):
        for each_pkg in each_result.get('Vulnerabilities'):
            pkg_list.append(each_pkg.get('PkgName'))

            pkg_list = list(set(pkg_list))
            pkg_list.sort()

            raw['Packages']['PkgList'] = pkg_list

    return raw


def select_package_cve(raw: dict) -> dict:
    print('-> Order by CVE.')
    cve_list = list()
    for each_result in raw.get('Packages').get('rawResults'):
        for each_pkg in each_result.get('Vulnerabilities'):
            cve_list.append(each_pkg.get('VulnerabilityID'))

            cve_list = list(set(cve_list))
            cve_list.sort()

            raw['Packages']['CVEList'] = cve_list

    return raw


def normalize_package_vulnerability(raw: dict) -> list:
    print('-> Normalize Packages Vulnerabilities.')
    all_vulnerabilities = list()
    installed = str()
    pkg_class = str()
    pkg_target = str()
    pkg_type = str()
    pkg_vulnerability = list()
    deep_raw = deepcopy(raw)

    for each_pkg in raw.get('Packages').get('PkgList'):
        for each_raw in deep_raw.get('Packages').get('rawResults'):
            for each_vuln in each_raw.get('Vulnerabilities'):
                if each_pkg == each_vuln.get('PkgName'):
                    pkg_target = each_raw.get('Target')
                    pkg_class = each_raw.get('Class')
                    pkg_type = each_raw.get('Type')
                    installed = each_vuln.get('InstalledVersion')
                    if 'PublishedDate' in each_vuln:
                        each_vuln['PublishedDate'] = datetime.strftime(
                            parser.parse(
                                each_vuln.get('PublishedDate')
                            ), date_format())
                        each_vuln['LastModifiedDate'] = datetime.strftime(
                            parser.parse(
                                each_vuln.get('LastModifiedDate')
                            ), date_format())
                    else:
                        each_vuln['PublishedDate'] = 'Not Informed'
                        each_vuln['LastModifiedDate'] = 'Not Informed'

                    each_vuln.pop('Layer')
                    each_vuln.pop('DataSource')
                    if 'CweIDs' in each_vuln:
                        each_vuln.pop('CweIDs')
                    each_vuln.pop('References')
                    each_vuln['CVSSScore'] = collect_score(each_vuln)
                    if 'CVSS' in each_vuln:
                        each_vuln.pop('CVSS')
                    pkg_vulnerability.append(each_vuln)

        all_vulnerabilities.append(
            {
                'PkgName': each_pkg,
                'PkgTarget': pkg_target,
                'PkgClass': pkg_class,
                'PkgType': pkg_type,
                'InstalledVersion': installed,
                'Vulnerabilities': pkg_vulnerability
            }
        )
        pkg_vulnerability = list()

    return all_vulnerabilities


def normalize_package_cve(raw: dict, vuln: list) -> list:
    print('-> Normalize Packages by CVE.')
    cve_list = list()
    pkg_path = str()

    for each_cve in raw.get('Packages').get('CVEList'):
        for each_result in vuln:
            for each_vuln in each_result.get('Vulnerabilities'):
                if each_cve == each_vuln.get('VulnerabilityID'):
                    if 'PkgPath' in each_vuln:
                        pkg_path = each_vuln.get('PkgPath')
                    cve_list.append(
                        {
                            'VulnerabilityID': each_vuln.get('VulnerabilityID'),
                            'PkgName': each_vuln.get('PkgName'),
                            'PkgPath': pkg_path,
                            'Title': each_vuln.get('Title'),
                            'InstalledVersion': each_vuln.get('InstalledVersion'),
                            'FixedVersion': each_vuln.get('FixedVersion'),
                            'PrimaryURL': each_vuln.get('PrimaryURL'),
                            'Severity': each_vuln.get('Severity'),
                            'Score': each_vuln.get('CVSSScore')
                        }
                    )

    return cve_list


def normalize_finding_secrets(raw: dict) -> list:
    print('-> Normalize Secrets Vulnerabilities.')
    all_secrets = list()
    categories = list()
    findings = list()
    target = str()

    for each_item in raw.get('Secrets').get('rawResults'):
        for each_secret in each_item.get('Secrets'):
            categories.append(each_secret.get('Category'))

    categories = list(set(categories))
    categories.sort()

    for each_category in categories:
        for each_item in raw.get('Secrets').get('rawResults'):
            for each_secret in each_item.get('Secrets'):
                if each_category == each_secret.get('Category'):
                    target = each_item.get('Target')
                    each_secret['Match'] = each_secret.get('Match').strip()
                    findings.append(each_secret)

        all_secrets.append(
            {
                'Category': each_category,
                'Target': target,
                'Secrets': findings
            }
        )

    return all_secrets


def collect_layers(raw: dict) -> list:
    print('-> Collect all container layers.')
    layers = list()
    if 'ImageConfig' in raw:
        for each_layer in raw.get('ImageConfig').get('history'):
            layers.append(each_layer.get('created_by'))
    else:
        return []

    return layers


def main(v: str, argument):
    print(f'-> Open file: {argument.source}')
    with open(argument.source, "rb") as open_file:
        data = load(open_file)
    open_file.close()

    global_info = scanner_type(data)
    scanner_data = split_info_scanner(data)
    scanner_data = concern_findings(scanner_data)
    scanner_data = select_package_name(scanner_data)
    scanner_data = select_package_cve(scanner_data)
    normalize_vuln = normalize_package_vulnerability(scanner_data)
    normalize_cve = normalize_package_cve(scanner_data, normalize_vuln)
    normalize_secrets = normalize_finding_secrets(scanner_data)

    env = Environment(
        loader=FileSystemLoader(f"{path.dirname(__file__)}/template/"),
        autoescape=select_autoescape()
    )
    template = env.get_template("index.html")

    print('-> Creating HTML file.')
    jinja_page = template.render(
        GlobalInfo=global_info,
        AppVersion=v,
        vulnerabilityTabTotalVuln=scanner_data.get('Packages').get('Resume'),
        vulnerabilityTabTotalSecrets=scanner_data.get('Secrets').get('Resume'),
        vulnerabilityTabGroup=normalize_vuln,
        vulnerabilityTabNoGroup=normalize_cve,
        layerTabLayers=scanner_data.get('Layers').get('rawResults'),
        secretsTabSecrets=normalize_secrets,
    )

    print('-> Saving HTML file.')
    with open(f'{argument.destination}.html', 'w', encoding='utf-8') as html_file:
        html_file.write(jinja_page)
    html_file.close()


if __name__ == '__main__':
    version = "0.1c"
    arguments = ArgumentParser(
        prog='Scanner Report',
        description='Normalize data from Trivy and create a HTML Report'
    )
    arguments.add_argument(
        '-s',
        '--source',
        action='store',
        required=True,
        help='Give the full path and name of file to be imported.'
    )

    arguments.add_argument(
        '-d',
        '--destination',
        action='store',
        required=True,
        help='Give the full path and name of file to be saved. This file will be saved in HTML.'
    )

    print('**** Starting scannerReport project.****')
    print(f'-> Version: {version}')

    args = arguments.parse_args()
    target_src = path.isfile(args.source)
    target_dst = path.isdir(path.split(args.destination)[0])

    print('-> Checking if the Source file exist.')
    if not target_src:
        print(f'--> Source file not found: {args.source}. Exit.')
        exit(0)
    else:
        print('--> Source File was found.')

    print('-> Checking if the Destination directory exist.')
    if not target_dst:
        print(f'--> Destination directory does not exist: {args.destination}. Exit.')
        exit(0)
    else:
        print('--> Destination directory exist.')

    main(version, args)
    print('**** Finish scannerReport project.****')
